mplcursors (0.6-1) unstable; urgency=medium

  * New upstream version 0.6
    - Remove upstream applied patch.
  * Update Standards-Version to 4.7.0

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Mon, 04 Nov 2024 20:36:40 +0000

mplcursors (0.5.3-1) unstable; urgency=medium

  * New upstream version 0.5.3
    - Remove upstream applied patches.
  * Apply upstream changes to fix tests failure. (Closes: #1063979, #1067255)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Thu, 18 Apr 2024 20:19:13 +0100

mplcursors (0.5.2-3) unstable; urgency=medium

  * Add upstream patch to use raw string and fix FTBFS. (Closes: #1058443)
  * Update Standards-Version to 4.6.2
    - No changes needed.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Fri, 22 Dec 2023 10:21:58 +0000

mplcursors (0.5.2-2) unstable; urgency=medium

  * Remove dependency on libopenblas. (Closes: #1028097)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Mon, 09 Jan 2023 20:34:59 +0000

mplcursors (0.5.2-1) unstable; urgency=medium

  * New upstream version 0.5.2
    - New version works with matplotlib/3.6.2. (Closes: #1027171)
  * Update Standards-Version to 4.6.1.0

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Thu, 05 Jan 2023 21:27:06 +0000

mplcursors (0.5.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.5.1
    Supports matplotlib > 3.5. Closes: #999518
  * d/gbp.conf: Set debian/master as default branch.
  * Change from transitional openblas package to libopenblas0. Closes: #994469
  * Update Standards-Version to 4.6.0.
  * Update copyright year.
  * d/source/options: Ignore .egg-info files. Prevent ftbfs when
    building twice in a row.
  * Drop build dependency on dh-sequence-python3, not needed.
  * autopkgtest:
    - Specify python3-all as dependency.
    - Drop dependencies specified in the binary package.
    - Don't check if AUTOPKGTEST_TMP exists.
  * At build time and when running autopkgtest, copy examples folder
    to enable more tests.

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sun, 28 Nov 2021 10:46:02 +0000

mplcursors (0.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sudip Mukherjee ]
  * New upstream version 0.4
    - Remove upstream applied patch.
  * Update Standards-Version to 4.5.1

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Sat, 26 Dec 2020 14:03:54 +0000

mplcursors (0.3-3) unstable; urgency=medium

  * Mark python team as maintainer
  * Move Vcs under python team in salsa.
  * Add salsa-ci.yml file to enable salsa pipelines.
  * Update from upstream master.
    - Updating fixes FTBFS. (Closes: #966995)
  * Use MPLCONFIGDIR to fix an autopkgtest.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Fri, 07 Aug 2020 20:04:29 +0100

mplcursors (0.3-2) unstable; urgency=medium

  * Source only upload.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Sun, 28 Jun 2020 22:28:45 +0100

mplcursors (0.3-1) unstable; urgency=medium

  * Initial release (Closes: #959870)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Mon, 11 May 2020 23:32:45 +0100
